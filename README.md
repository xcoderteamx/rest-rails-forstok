# RAILS 5 REST API FORSTOK

## INSTALL DEPENDENCIES
```bash
$ bundle install
```

## SETUP DATABASE
run mysql

created db name forstok

open config
```bash
config/database.yml
```

setting
```bash
username:
password:
```


## RUN MIGRATION
```bash
$ rails db:migrate
```

## RUN SERVER
```bash
$ rails s
```
## TEST WITH POSTMAN

```bash
https://www.getpostman.com/collections/ecb860932eae9adbe56a
```
