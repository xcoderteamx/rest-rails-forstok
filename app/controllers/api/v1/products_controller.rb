
module Api
  module V1
    class ProductsController < ApplicationController
      require 'Variant'
      def index
        if params[:search]
            products = Product.where('name LIKE ? OR description LIKE ?', "%#{params[:name]}%", "%#{params[:name]}%")
            variants = Variant.where('sku LIKE ?', params[:sku])
            products_data = []
              products.each do |product_params|
                products_data = [
                  :name => product_params[:name],
                  :description => product_params[:description],
                  :category => product_params[:category],
                  :brand => product_params[:brand],
                  :variants => variants
                 ]
              end

            if products
              render json: {status: 'success', message:'get by product name & sku', data:products_data}, status: :ok
            else
              render json: {status: 'errors', message:'product empty', data:''}, status: :unprocessable_entity
            end

        else
          products = Product.all
          render json: {status: 'success', message:'get all products', data:products}, include: 'variants', status: :ok
        end

      end

      def show
        product = Product.find(params[:id])
        render json: {status: 'success', message:'get by id product', data:product}, include: 'variants', status: :ok
      end

      def create
        product = Product.new(product_params)
        variants = params[:variants]
        if product.save
            params[:variants].each do |p|
              Variant.new(
                :product_id => product.id,
                :quantity => p['quantity'],
                :sku => p['sku'],
                :price => p['price'],
                :sale_price => p['sale_price'],
                :sale_start_at => p['sale_start_at'],
                :sale_end_at => p['sale_end_at'],
                :new_condition => p['new_condition'],
                :status => 'order',
              ).save
            end
          render json: {status: 'success', message:'save product', data:product}, status: :ok
        else
          render json: {status: 'errors', message:'product not saved', data:product.errors}, status: :unprocessable_entity
        end
      end

      def destroy
        product = Product.find(params[:id])
        if product.destroy
          Variant.where(:product_id => params[:id]).delete_all
        end
        render json: {status: 'success', message:'deleted product', data:product}, status: :ok
      end

      def update
        if params[:status]
          varian = Variant.find_by_sku(params[:sku])
          # varian.update_attributes(variant_params)
          render json: {status: 'success', message:'update status sku', data:params[:sku]}, status: :ok
        else
          product = Product.find(params[:id])
          if product.update_attributes(product_params)
            params[:variants].each do |p|
              varian = Variant.find_by_product_id(params[:id])
              varian.update_attributes(
                :quantity => p['quantity'],
                :sku => p['sku'],
                :price => p['price'],
                :sale_price => p['sale_price'],
                :sale_start_at => p['sale_start_at'],
                :sale_end_at => p['sale_end_at'],
                :new_condition => p['new_condition'],
              )
            end
            render json: {status: 'success', message:'updated product', data:product}, status: :ok
          else
            render json: {status: 'errors', message:'product not updated', data:product.errors}, status: :unprocessable_entity
          end
        end
      end

      private

      def product_params
        params.permit(:id, :product, :name, :description, :category, :brand, :variants)
      end

      def variant_params
        params.permit(:id, :product, :quantity, :sku, :price, :sale_price, :sale_start_at, :sale_end_at, :new_condition, :status)
      end

    end
  end
end
