class Product < ApplicationRecord
  has_many :variants
  accepts_nested_attributes_for :variants
  validates :name, presence: true
  validates :description, presence: true
  validates :category, presence: true
  validates :brand, presence: true
end
