5.times do
  Product.create({
      name: Faker::Name.name,
      description: Faker::Lorem.sentence,
      category: Faker::Lorem.sentence,
      brand: Faker::Lorem.sentence,
  })

  Variant.create({
      product_id: Faker::Number.number(10),
      quantity: Faker::Number.number(10),
      sku: Faker::Code.ean,
      price: Faker::Number.number(10),
      sale_price: Faker::Number.number(10),
      sale_start_at: Faker::Date.sale_start_at,
      sale_end_at: Faker::Date.sale_end_at,
      new_condition: Faker::Types.character,
      status: Faker::Types.character,
  })

end
