class CreateVariants < ActiveRecord::Migration[5.1]
  def change
    create_table :variants do |t|
        t.string "product_id"
       	t.string "quantity"
  	    t.text "sku"
  	    t.string "price"
  	    t.string "sale_price"
  	    t.date "sale_start_at"
        t.date "sale_end_at"
        t.string "new_condition"
        t.string "status"
    end
  end
end
